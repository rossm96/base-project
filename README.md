# README #

To setup the project, run 


```
#!command line

git clone https://rossm96@bitbucket.org/rossm96/base-project.git
```

This repository is for getting started quickly with simple front end projects and exercises.

## To compile compass 

It comes setup with bootstrap, sass with compass integration.

Simply navigate to the directory in terminal and run


```
#!command line

compass watch
```